#!/bin/env python

"""
This script handles the execution of scripts defined in the passed json file.

Usage:
./handle_process.py <json_file_name> [action] [other_parameters_list]

TIP: If this app is executed through an ssh connection, we recommend running 
the script using below approach, so if the connection is lost, script will 
continue running:

nohup ./handle_process.py <arguments> &

 - json_file_name: the name of the json file to be handled by this app.
        It can be the complete name of the file, or just the name without the 
        extension. If only the name is passed in, this app will look for a 
        file with that name and .json extension. File can have any extension 
        if its full name is provided, it only needs to have valid json content.
        Param is case-sensitive. Please note the specified file should resides 
        inside a folder named conf, on the same directory this script resides:
            .
            ..
            handle_process.py
            conf/
                |---- test.json
                |---- other.json
                |---- noext

 - action: specific action to be executed. Even if the action is not listed 
        as available to be executed (see below explanation of json settings), 
        this app will try to run only the specific actions defined under this 
        passed one. If this param is not specified, all available actions 
        will be executed.
 - other_parameters_list: Any amount of parameters used by the scripts defined 
        in the json file. All of them should have the form: -param value.
        This list is not used at all by the logic of this script, but by the 
        scripts defined as specific actions.

How this works?

This script depends totally on the json file passed in as the first parameter, 
as it executes whatever is defined in the json.

json file must have at least the key 'settings', and the key 'actions' inside 
it, which is the list of the actions available for execution (it could be an 
empty list, in which case no action is available for running if no action was 
specified as asgument).

The actions will be executed in the order they appear on the list, and could 
contain numbers, representing the seconds this app will sleep. This option is 
helpful for waiting a time between actions. Please note the actions 
represented by numbers (under this list) will be treated as 'time to wait'.

Also inside 'settings', key 'email' could be defined, which takes a list of 
email addresses as its value. When this app finishes running, or it's 
terminated because of an error, an email will be sent to every one of the 
addresses listed under 'email'. If it's not desired to send an email, just 
don't define this key, or define it with an empty list as its value.

Apart from 'settings', all other keys defined at the first level of the json 
are actions, executed only if they are specified in the 'settings -> actions' 
list explained before, or if a specific action is passed in as parameter, in 
which case it will be executed without checking the settings.

Each action key should have a list as its value. Every list has more 
specific actions (real actions, executable scripts), which will be executed in 
the order they appear on the list (though execution of the specific actions 
depends on another value, see below).

For every specific action, these keys are available:
    - execute (mandatory): accepts only true or false. It defines if the 
        specific action will be executed.
    - script_path (mandatory): defines the path of the script to be executed 
        as the specific action.
    - script (mandatory): defines que script to be executed as the specific 
        action. If the script needs static parameters, they should be passed 
        in here. If the script needs dynamic parameters, they should be passed 
        through 'other_parameters_list' of the app, and then they should be 
        mapped using the option 'params' (explained below). Please note the 
        script must be defined the same way it's used to execute it:
            ./script.sh [static_params]  # note the ./
            /bin/bash script.sh [static_params]
            python script.py [static_params]
    - server (optional): name of the server where this app should connect to 
        in order to execute the specific action, defined by 'script' value. If 
        this key is not present, or its value is an empty string, this app 
        will execute 'script' in the current server.
    - username (optional; mandatory if 'server' key was defined and 'execute' 
        is true): username to be used for connecting via ssh to the server 
        specified in 'server'.
    - params (optional): list containing the names of the dynamic params 
        needed for executing 'script'.
        i.e. let's say this app was called this way: 
            ./handle_process.py switch -dc atl
        If the specific action ('script') needs the dynamic param '-dc' passed 
        in, then 'params' can be defined as this:
            "params": ["-dc"]
        so the value passed in for '-dc', will be passed over as parameter of 
        this specific action.
    - wait_for_response (optional): value is another dictionary with these 
        available keys:
        - response (mandatory): accepts a text, which is the expected result 
            from the execution of the current specific action. It could be 
            left empty, in which case this app will wait for this specific 
            script to finish, but it won't check the output. Please note the 
            result this app checks out, is the one returned by the 'exit' 
            function in Bash and Python, because by using 'echo' (in Bash), 
            and 'print' (in Python), the resulting output is the concatenation 
            of all 'echoed' or 'printed' strings. 
        - exit_on_failure (mandatory): accepts true or false. Defines whether 
            the execution of this app should be stopped on the case the 
            'response' was not the expected one. Even when this key is 
            mandatory, it won't be checked if 'response' key is left empty.
        - script_path_on_failure (optional): defines the path of the script to 
            be run only if the 'response' was not the expected one.
        - script_on_failure (optional; mandatory if 'script_path_on_failure' 
            was defined): script to be run only if 'response' was not the 
            expected one, and 'script_path_on_failure' is defined. If script 
            needs fixed parameters, just pass them in here; if it needs 
            dynamic parameters, pass them through 'other_parameters_list' 
            argument of this app. This 'failure' script will always be run 
            from the current server. Please note the execution of this script 
            doesn't depend on the value of 'exit_on_failure'; even if 
            'exit_on_failure' is defined as 'true', this script will be 
            executed, and then the app will exit afterwards.
        - params_on_failure (optional): list containing the params that must 
            be passed in to the 'script_on_failure'. Params names match those 
            passed in to the main app when called.

This script sends log text to a log file under ./logs/ directory. Any log 
resulting from the scripts of the specific actions should be managed by those 
specific scripts.

IMPORTANT: Please be advised this app doesn't catch the errors coming from the 
execution of the specific actions if the response is not set to be analized.

TIP: To find out if a specific action was successfully run, define the key 
'wait_for_response' and set a value to 'response' (any value), so this app 
will wait for the specific action to finish executing and will send to the log 
file any error ocurred when running the script for the specific action.
"""

from datetime import datetime
from os import path
import sys
from time import sleep

import subprocess

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from pssapi.cmdexe import cmd
from pssapi.logger import logger as logmod
from pssapi.utils import conf, conn, decor, util

logger = None
emails = None

EMAIL_SENDER = "ProductionSystemsSupport@tracfone.com"

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE_NAME = path.join(EXEC_DIR, 'logs', 'handle_process.log')
APP_CONF_DIR = path.join(EXEC_DIR, 'conf')

MANDATORY_KEYS_SPECIFIC_ACT = ['execute', 'script_path', 'script']
MANDATORY_KEYS_FAILURE = ['response', 'exit_on_failure']
EXPECTED_BOOL_VALS = [True, False]

def _prepare_email(error, **kwargs):
    """Create and return a dictionary with the fields needed to send an email. 

    Keyword arguments:
    error -- error message, in case the email is because of an error. Pass 
    False or None if the email if not because of an error.
    kwargs -- dictionary with the values needed for the email. Not necessary 
    if preparing an email because of an error. If not, these keys are 
    mandatory:
        full_params: arguments passed in to this app when called.
        init_time: exact datetime this app started.
        end_time: exact datetime this app finished running.
    """
    subject_template = "Handle Process - {0}"
    if error:
        subject = subject_template.format("ERROR!")
        body = ("Handle Process application stopped executing due to this " +  
                "error:\n\n\t{0}\n\n").format(error)
        body += ("This is only an FYI. ")
        high = True
    else:
        full_params = kwargs['full_params']
        init_time = kwargs['init_time']
        end_time = kwargs['end_time']
        # subject is conformed like this:
        # Handle Process - "<conf_file> [action]"
        conf_file = full_params[1]
        action = " {0}".format(full_params[2]) if len(full_params) > 2 else ""
        subject_options = "\"{0}{1}\"".format(conf_file, action)
        subject = subject_template.format(subject_options)
        body = ("""Handle Process application finished processing successfully.
                \nOriginal call: {0}\nStarted: {1}\nFinished: {2}\n\n""").\
                format(" ".join(full_params), init_time, end_time)
        high = False
    body += ("Please check log file for details.\n")
    body += ("Server: {0}".format(conn.current_server()))
    return {'subject': subject, 'body': body, 'high': high}

def _send_email(**kwargs):
    """Send an email specified by coming arguments.
    kwargs should have these keys:
        - dest: list containing the addresses to send the email to.
        - subject: subject of the email.
        - body: body of the email.
    This key is optional:
        - high: if the message should be sent with high priority. False by 
        default.
    """
    error = None
    try:
        from pssapi.emailer import emailer
        if emails:
            logger.info("Sending email to: {0}".format(emails))
            subject = kwargs['subject']
            body = kwargs['body']
            priority = False
            if 'high' in kwargs:
                priority = kwargs['high']
            res = emailer.send_email(subject=subject, body=body, 
                                     sender=EMAIL_SENDER, dest=emails, 
                                     highpriority=priority)
            if res != "OK":
                error = res
    except Exception as e:
        error = str(e)
    if error:
        logger.info("ERROR when trying to send email: {0}".format(error))

def _quit_script(error_message=None):
    """Print error_message into log file. Quit current script."""
    if error_message:
        logger.error(error_message)
        email_kwargs = _prepare_email(error=error_message)
        _send_email(**email_kwargs)
        print({'message': error_message})
    else:
        print({'message': 'OK'})
    logger.info("Quiting now... Bye!\n")
    exit()

def _convert2int(txt):
    """Try to convert 'txt' to an integer."""
    try:
        return True, int(txt)
    except:
        return False, 0

def _check_dynamic_params(dparams, script_params):
    """Check the arguments passed to this app are part of the params defined 
    in the json conf file.

    Keyword arguments:
    dparams -- list of arguments passed to this app.
    script_params -- list of parameters defined in the json conf file.
    """
    if not set(dparams) <= set(script_params):
        _quit_script(("Parameters passed to this script don't cover all " + 
                      "parameters expected by specific actions."))

def _check_specific_wait_for_action(specific_act_conf, script_params):
    """Check if the configuration for the action includes 'wait_for_response', 
    in which case check all mandatory keys are defined, and the list of 
    'script_params' passed to this app matches the list of params defined in 
    'specific_act_conf'.
    """
    if 'wait_for_response' in list(specific_act_conf.keys()):
        if not set(MANDATORY_KEYS_FAILURE) <= \
                set(specific_act_conf['wait_for_response']):
            _quit_script(("Specific action conf is missing mandatory key(s)" + 
                " when key 'wait_for_response' is defined."))
        specific_act_wait_conf = specific_act_conf['wait_for_response']
        if 'params_on_failure' in specific_act_wait_conf:
            _check_dynamic_params(specific_act_wait_conf['params_on_failure'], 
                                  script_params)
        if specific_act_wait_conf['exit_on_failure'] not in EXPECTED_BOOL_VALS:
            _quit_script("Boolean value was expected for key 'exit_on_failure'.")
        if ('script_path_on_failure' in specific_act_wait_conf and \
            'script_on_failure' not in specific_act_wait_conf) or \
           ('script_on_failure' in specific_act_wait_conf and \
            'script_path_on_failure' not in specific_act_wait_conf):
            _quit_script(("Missing either key 'script_path_on_failure' " + 
                          "or key 'script_on_failure'."))

def _check_specific_action(specific_action_conf, script_params):
    """Check 'specific_act_conf' dictionary has the mandatory keys on it, and 
    'script_params' matches the arguments passed to this app.
    If any condition is not met, quit this app.
    """
    if not isinstance(specific_action_conf, dict):
        _quit_script("Specific action not defined as dictionary.")
    specific_act_name_key = specific_action_conf.keys()
    if len(specific_act_name_key) != 1:
        _quit_script("Specific action has no name, or more than one.")
    specific_act_name = list(specific_act_name_key)[0]
    logger.info("Checking specific action: '{0}'".format(specific_act_name))
    if not isinstance(specific_action_conf[specific_act_name], dict):
        _quit_script("Specific action configuration not defined as dictionary.")
    specific_act_conf = specific_action_conf[specific_act_name]
    if not set(MANDATORY_KEYS_SPECIFIC_ACT) <= set(specific_act_conf):
        _quit_script("Specific action conf is missing mandatory key(s).")
    execute_this_action = specific_act_conf['execute']
    if execute_this_action not in EXPECTED_BOOL_VALS:
        _quit_script("Boolean value was expected for key 'execute'.")
    if ('server' in specific_act_conf and specific_act_conf['server'] and \
            execute_this_action and ('username' not in specific_act_conf or \
            not specific_act_conf['username'])):
        _quit_script("Can not specify server without username.")
    if not execute_this_action:
        logger.info("Specific action '{0}' won't be executed.".format(
                                                            specific_act_name))
    else:
        if 'params' in specific_act_conf:
            _check_dynamic_params(specific_act_conf['params'], script_params)
        # check actions defined as 'wait_for_response'
        _check_specific_wait_for_action(specific_act_conf, script_params)

def _check_conf(confdata, action, scripts_params):
    """Check confdata is a json with the mandatory key/value data."""
    logger.info("Checking conf file...")
    main_keys = confdata.keys()
    if 'settings' not in main_keys:
        _quit_script("Key 'settings' is missing in conf file.")
    if 'actions' not in confdata['settings'] or \
            not isinstance(confdata['settings']['actions'], list):
        _quit_script("Key 'actions' is missing, or it's not a list.")
    if 'email' in confdata['settings'] and \
            not isinstance(confdata['settings']['email'], list):
        _quit_script("Key 'email' not defined as a list.")
    if action:
        if action not in main_keys or not isinstance(confdata[action], list):
            _quit_script(("Action '{0}' is not defined in the conf file, " + 
                          "or its value is not a list.").format(action))
        logger.info("Checking action: {0}".format(action))
        for specific_action in confdata[action]:
            _check_specific_action(specific_action, scripts_params)
    else:
        available_actions = confdata['settings']['actions']
        for act in available_actions:
            logger.info("Checking action: {0}".format(act))
            isint, num = _convert2int(act)
            if not isint:
                act_conf = confdata[act]
                for specific_act in act_conf:
                    _check_specific_action(specific_act, scripts_params)
            elif num < 0:
                _quit_script("Bad sleeping time defined in available actions.")

def _get_conf(file_option):
    """Get configuration data and return it. Quit script if data couldn't 
    be retrieved.
    """
    logger.info("Getting configuration...")
    file_name = path.join(APP_CONF_DIR, file_option)
    if path.isfile(file_name):
        confdata = conf.get_conf(file_name)
    elif path.isfile("{0}.json".format(file_name)):
        confdata = conf.get_conf("{0}.json".format(file_name))
    else:
        _quit_script(("No file named either {0} or {1}").\
            format(file_option, "{0}.json".format(file_option)))
    if isinstance(confdata, dict):
        return confdata
    _quit_script(confdata)

def _init():
    """Load the logger object and print initial text to the log."""
    args = sys.argv
    global logger
    logger_fmtter = {
        'ERROR': '%(asctime)s - %(levelname)s - %(message)s',
        'DEFAULT': '%(asctime)s - %(message)s'
    }
    logger = logmod.load_logger(log_file_name=LOG_FILE_NAME, **logger_fmtter)
    logger.info("I was called this way: {0}".format(args))
    logger.info("Running on server: {0}".format(conn.current_server()))

def _customize_log(msg, action):
    """Log msg specifying the action in which it is executed."""
    logger.info("({0}): {1}".format(action, msg))

def _create_command(script_path, script, settings, app_params, failure=False):
    """Create a return command to be executed as part of executing 'script' 
    for a specific action.

    Keyword arguments:
    script_path -- path where 'script' is residing.
    script -- script to be executed.
    settings -- dictionary containing the conf for getting the script params.
    app_args -- list of arguments passes to this app.
    failure -- defines if the command is being created for failure purposes.
    """
    command = "cd {0}; {1}".format(script_path, script)
    # add dynamic parameters to the command
    params = []
    if not failure and 'params' in settings and settings['params']:
        params = settings['params']
    elif failure and 'params_on_failure' in settings and \
            settings['params_on_failure']:
        params = settings['params_on_failure']
    for param in params:
        command += (" {0}".format(app_params[param]))
    # add ssh connection if servers are different
    if 'server' in settings and settings['server']:
        thisserver = conn.current_server()
        if thisserver != settings['server'].lower():
            server = settings['server'].lower()
            username = settings['username'].lower()
            command = "ssh {0}@{1} \"{2}\"".format(username, server, command)
    return command

def _action_on_failure(expected_output, output, stderr, action, fail_settings, 
                       app_arguments):
    """Execute the 'failure script' when the regular script doesn't return the 
    expected value.

    Quit this app if so is defined on 'fail_settings'.

    Keyword arguments:
    expected_output -- value defined in the json conf file, which is the one 
    this app is expecting to get if everything was OK when executing a script.
    output -- actual value resulted from the execution of a script.
    stderr -- any error resulted from the execution of a script.
    action -- current general action on execution.
    fail_settings -- settings defined in the json conf file for when the 
    'wait_for_response' is available.
    app_args -- arguments passed to this app. They are used to create the 
    command. 
    """
    logmsg = ("WARNING - output was not the expected one.\n" + 
                "Expected: {0}\nOutput: {1}").\
                    format(expected_output, output)
    _customize_log(logmsg, action)
    # if error is received, send it to the log
    errop = stderr.replace('\r', '').replace('\n', '').strip()
    if errop:
        logmsg = ("WARNING - this error was received from " + 
                    "executing the script: {0}").format(errop)
        _customize_log(logmsg, action)
    # check failure script exists and execute it
    if 'script_path_on_failure' in fail_settings and \
            'script_on_failure' in fail_settings and \
            fail_settings['script_path_on_failure'] and \
            fail_settings['script_on_failure']:
        # execute on-failure script
        f_script = fail_settings['script_on_failure']
        f_script_path = fail_settings['script_path_on_failure']
        command = _create_command(f_script_path, f_script, fail_settings, 
                                  app_arguments, failure=True)
        logmsg = ("executing 'failure script' command: {0}").format(command)
        _customize_log(logmsg, action)
        subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # check if quiting on failure
    if fail_settings['exit_on_failure']:
        _quit_script("Exit on failure was set to 'true'.")

def _exec_wait_for_response_action(action, act_settings, command, app_args):
    """Execute action defined as 'wait_for_response' in the json conf file.
    If the expected value doesn't match the resulted one, execute script 
    defined for failure.

    Keyword arguments:
    action -- general action currently on execution
    act_settings -- settings defined for the action in the json conf file.
    command -- complete command, including script and ssh part (if needed), to 
    be executed as the specific action. This execution is the one this app 
    will be waiting the output from.
    """
    _customize_log("waiting for response...", action)
    wfresp_settings = act_settings['wait_for_response']
    expected_response = wfresp_settings['response']
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    dummy, stderr = p.communicate()
    if expected_response != "":
        # returned code was used here as using the result from 'echo' in bash 
        # or 'print' in python, will concatenate all messages printed 
        # during script execution
        rc = p.returncode
        if str(rc) != str(expected_response):
            _action_on_failure(expected_response, rc, stderr, action, 
                               wfresp_settings, app_args)
        else:
            _customize_log("response received: {0}".format(rc), action)
    else:
        _customize_log("response won't be checked out...", action)

def _exec_action(specific_action_conf, action, app_args):
    """Execute specific action defined as part of the more general 'action'."""
    specific_action_name = list(specific_action_conf.keys())[0]
    act_settings = specific_action_conf[specific_action_name]
    if act_settings['execute']:
        _customize_log("processing '{0}'".format(specific_action_name), action)
        script_path = act_settings['script_path']
        script = act_settings['script']
        command = _create_command(script_path, script, act_settings, app_args)
        logmsg = "executing command: {0}".format(command)
        _customize_log(logmsg, action)
        if 'wait_for_response' in act_settings:
            _exec_wait_for_response_action(action, act_settings, command, 
                                           app_args)
        else:
            subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    else:
        _customize_log(("NOT executing specific action '{0}': configured as" + 
                    " 'execute = False'").format(specific_action_name), action)

def _handle_process(actions_conf, action, app_args):
    """Handle the process defined by the 'action' key in 'actions_conf'.

    Keyword arguments:
    actions_conf -- list of specific actions coming from the json conf file, 
    and defined under the key passed as 'action' value.
    action -- general action specify as main key in the json conf file.
    app_args -- dictionary of arguments passed to this app, which will be 
    passed to the scripts defined as specific actions.
    """
    global emails
    emails = []
    if 'email' in actions_conf['settings']:
        emails = actions_conf['settings']['email']
    actions2run = [action] if action else actions_conf['settings']['actions']
    for gen_act in actions2run:
        isint, seconds = _convert2int(gen_act)
        if isint:
            logger.info("Sleeping {0} seconds...".format(seconds))
            sleep(seconds)
        else:
            logger.info("Processing {0}...".format(gen_act))
            gen_act_conf = actions_conf[gen_act]
            for specific_action_conf in gen_act_conf:
                _exec_action(specific_action_conf, gen_act, app_args)

def _parse_params(fullparams):
    """Parse fullparams and return a tuple containing the name of the file to 
    be used as json content, the action to execute, and a dictionary with 
    the parameters for the specific actions inside the json file.
    """
    logger.info("Parsing parameters...")
    lenfullparams = len(fullparams)
    if lenfullparams == 1:
        _quit_script("Not enough parameters passed in.")
    file_option = fullparams[1]
    other_params = {}
    action = ''
    if lenfullparams > 2:
        if '-' not in fullparams[2]:
            action = fullparams[2]
        if lenfullparams > 3:
            fullparams = fullparams[3:] if action else fullparams[2:]
            expecting_param_identifier = True
            temp_param_identifier = ""
            for param in fullparams:
                if expecting_param_identifier:
                    if '-' not in param or len(param) == 1 or param[0] != '-':
                        _quit_script("Parameter(s) not valid.")
                    temp_param_identifier = param
                    expecting_param_identifier = False
                else:
                    if temp_param_identifier in other_params.keys():
                        _quit_script("Param identifier is duplicated.")
                    other_params[temp_param_identifier] = param
                    expecting_param_identifier = True
    return (file_option, action, other_params)

def _app_terminated(*args):
    """Log message to the logs stating this app was terminated."""
    error_msg = "Application was interrupted."
    _quit_script(error_msg)

@decor.onterminated(_app_terminated)
def _main():
    init_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    error = None
    try:
        _init()
        fullparams = sys.argv
        file_option, action, other_params = _parse_params(fullparams)
        confdata = _get_conf(file_option)
        _check_conf(confdata, action, other_params)
        _handle_process(confdata, action, other_params)
        end_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        email_kwargs = {
            'full_params': fullparams, 
            'init_time': init_date, 
            'end_time': end_date
        }
        email_kwargs = _prepare_email(error=False, **email_kwargs)
        _send_email(**email_kwargs)
    except Exception as e:
        error = str(e)
    _quit_script(error_message=error)

_main()
